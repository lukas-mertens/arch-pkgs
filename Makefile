all::
	zsh -c "source build.sh && PrepareEnv && BuildAllPackages"
prepare::
	zsh -c "source build.sh && PrepareEnv && GeneratePipeline"
build::
	zsh -c "source build.sh && BuildAllPackages"
repo::
	zsh -c "source build.sh && GenerateRepo"
clean::
	rm -rf public
	rm -rf aur
	rm -rf **/*.tar.xz
	rm -rf **/src
	rm -rf **/pkg
