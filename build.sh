#!/bin/zsh
#current working dir
WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DEBUG=false

# register top level shell to be able to fail from inner functions
trap "exit 1" TERM
export TOP_PID=$$

# name of the repo displayed in pacman
REPONAME="lukmert"

# list of colorcodes for printouts
DEBUGHIGHLIGHT="\u001b[1m\u001b[31m"     # Debug information
MESSAGEHIGHLIGHT="\u001b[1m\u001b[36m"   # Default messages
RESETHIGHLIGHT="\u001b[37m\u001b[0m"     # Reset to default color of terminal
SUCCESFULHIGHLIGHT="\u001b[1m\u001b[32m" # Job terminated successfully
ERRORHIGHLIGHT="\u001b[1m\u001b[31m" # Job terminated with errors

# message "colorcode" "text to be displayed"
function Message() {
	printf "$1"
	shift
	TEXT="$@"
	LEN="${#TEXT}"

	printf '=%.0s' $(eval echo "{1..$LEN}")
	printf "\n"
	echo -e "$@"
	printf '=%.0s' $(eval echo "{1..$LEN}")
	printf "\n"
	printf "$RESETHIGHLIGHT"
}

# main function to recursively create PKGBUILDS
function BuildPackage() {
	(
	DEPENDENCIES=$(yay -Si $(basename $1) | sed -n '/Depends\ On/,/:/p'|sed '$d'|cut -d: -f2)
	Message "$MESSAGEHIGHLIGHT" "Starting building of $1"
	Message "$MESSAGEHIGHLIGHT" "Installing dependencies for build $1: $DEPENDENCIES"
	yay -Syu $DEPENDENCIES --noconfirm
	cd $WDIR && mkdir -p public && cd $1
	if [[ $1 == *"aur/"* ]]; then
		if [ "$DEBUG" = true ] ; then
			$(makepkg -s -f --noconfirm)
		else
			$(makepkg -s -f --noconfirm 1> /dev/null)
		fi
	else
		if [ "$DEBUG" = true ] ; then
			$(makepkg -d -s -f --noconfirm)
		else
			$(makepkg -d -s -f --noconfirm 1> /dev/null)
		fi
	fi
	result=$?
	if [ "$DEBUG" = true ] ; then
		ls -l
	fi
	cd $WDIR
	if [ "$DEBUG" = true ] ; then
		echo "Before delete..."
		tree
	fi
	echo "moving $(ls **/*.tar.zst)" && mv **/*.tar.zst public -n -v
	cd $WDIR
	cd aur && mv **/*.tar.zst ../public -n -v
	cd $WDIR
	cd external && mv **/*.tar.zst ../public -n -v
	cd $WDIR
	rm -rf $1
	if [ "$DEBUG" = true ] ; then
		echo "After delete..."
		tree
	fi
	space=$(df -h . | tail -1 | awk '{print $4}')
	if [ "$result" -eq "0" ]; then
		Message "$SUCCESFULHIGHLIGHT" "Finished building of $1"
	else
		Message "$ERRORHIGHLIGHT" "Building of $1 failed. Exit code $result"
		kill -s TERM $TOP_PID
	fi
	Message "$SUCCESFULHIGHLIGHT" "Space left on device: $space"
	)
}

function PrepareEnv() {
	if [ "$DEBUG" = true ] ; then
		Message "$DEBUGHIGHLIGHT" "Debug mode on"
	fi

	# clean up
	cd $WDIR
	rm -rf public
	rm -rf aur
	rm -rf external
	rm -rf nodejs

	# create public for later
	mkdir public -p

	# download aur
	mkdir aur -p
	cd aur && cat ../aur-packages.conf | grep "^[^#;]" | xargs yay -G
	Message "$SUCCESFULHIGHLIGHT" "Finished downloading aur PKGBUILDs.."

	#download external PKGBUILDS
	cd $WDIR
	mkdir -p external
	cd external
	cat ../external-packages.conf | grep "^[^#;]" > /tmp/external-filtered.conf
	while read p; do
		mkdir $(echo "$p" | cut -d' ' -f1)
		cd $(echo "$p" | cut -d' ' -f1)
		curl -O $(echo "$p" | cut -d' ' -f2)
		cd $WDIR/external
	done < /tmp/external-filtered.conf
	Message "$SUCCESFULHIGHLIGHT" "Finished downloading external PKGBUILDs..."
	cd $WDIR

	# delete .git directory in aur packages to pass to next jobs
	rm -rf aur/**/.git

	# generate node packages
	Message "$MESSAGEHIGHLIGHT" "Starting generation of node pkgbuilds"
	mkdir nodejs -p
	cd nodejs
	for pkgname in $(cat ../node-packages.conf | grep "^[^#;]"); do
		escapedPackageName=${pkgname/\//-}
		mkdir -p $escapedPackageName
		npm2PKGBUILD $pkgname > $escapedPackageName/PKGBUILD
		perl -pi -e "s:pkgname=nodejs-$pkgname:pkgname=nodejs-$escapedPackageName:g" $escapedPackageName/PKGBUILD
		sed -i -e "s|pkgname.*$pkgname|pkgname=nodejs-$escapedPackageName|g" $escapedPackageName/PKGBUILD
		sed -i -e "s|/-/\$_npmname|/-/\$(basename \$_npmname)|g" $escapedPackageName/PKGBUILD
	done
	Message "$SUCCESFULHIGHLIGHT" "Finished generating all node packages.."
	cd $WDIR

	if [ "$DEBUG" = true ] ; then
		cd $WDIR && tree
	fi
}

function GeneratePipeline() {
	rm -rf child-pipeline.yml
	cat <<EOF >> child-pipeline.yml
image: registry.gitlab.com/lukas-mertens/arch-build-docker
stages:
 - child_prepare
 - child_build
 - child_deploy

child_prepare_env:
  stage: child_prepare
  before_script:
    - shopt -s nullglob
  script:
    - yay -Syy
    - make prepare
  artifacts:
    untracked: true
    paths:
      - debuglog
    when: always
EOF

for dir in `find $WDIR -name PKGBUILD -type f | sed -r 's|/[^/]+$||' |sort |uniq`
do
	pkgname=$(basename $dir)
	cat <<EOF >> child-pipeline.yml
build_${pkgname}:
  stage: child_build
  artifacts:
    paths:
      - public
  dependencies:
   - child_prepare_env
  script:
   - zsh -c "source build.sh && BuildPackage ${dir}"

EOF
	done

	cat <<EOF >> child-pipeline.yml
child_deploy:
  stage: child_deploy
  only:
   - master
  before_script:
   - shopt -s nullglob
   - curl https://dl.min.io/client/mc/release/linux-amd64/mc --output mc
   - chmod +x mc
  script:
   - tree
   - ./mc config host add lukmert_minio https://minio.lukmert.de minio $MINIO_PASSWORD
   - ./mc ls lukmert_minio
   - make repo
   - ./mc rm --recursive --force lukmert_minio/arch-pkgs || true
   - ./mc mirror public/ lukmert_minio/arch-pkgs # upload to bucket (ignore missing override permissions)
  dependencies:
EOF

for dir in `find $WDIR -name PKGBUILD -type f | sed -r 's|/[^/]+$||' |sort |uniq`
do
	pkgname=$(basename $dir)
	cat <<EOF >> child-pipeline.yml
  - build_${pkgname}
EOF
	done

}

function BuildAllPackages() {
	for dir in `find $WDIR -name PKGBUILD -type f | sed -r 's|/[^/]+$||' |sort |uniq`
	do
		# for async build (unstable):
		# BuildPackage $dir &
		# for synchronious build:
		BuildPackage $dir
	done

	wait
	Message "$SUCCESFULHIGHLIGHT" "Finished building all packages..."
	cd $WDIR

	# print out tree after debuggung if debugging is on
	if [ "$DEBUG" = true ] ; then
		cd $WDIR && tree
	fi
}

function GenerateRepo() {
	repo-add public/$REPONAME.db.tar.gz public/*.pkg.tar.zst
	cd public && tree -H '.' -L 1 --noreport --charset utf-8 > index.html

	# print out tree at the end if debugging is on
	if [ "$DEBUG" = true ] ; then
		cd $WDIR && tree
	fi
}
