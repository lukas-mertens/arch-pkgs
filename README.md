If you want to add this repository to your system, add
```
[lukmert]
SigLevel = Optional TrustAll
Server = https://arch-pkgs.lukmert.de
```
to your `/etc/pacman.conf`.
